---
layout: handbook-page-toc
title: TAM Segments
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## Overview

The Technical Account Manager organization is distributed across different customer segments, based on customer size and Annual Recurring Revenue.

## Segment criteria

Segment criteria are [defined in the Technical Account Manager wiki](https://gitlab.com/gitlab-com/customer-success/tam/-/wikis/Segments).

## Segments

[<button class="btn btn-primary" type="button">Digital Touch</button>](digital/)
[<button class="btn btn-primary" type="button">Scale</button>](scale/)
[<button class="btn btn-primary" type="button">Mid-Touch</button>](mid-touch/)
[<button class="btn btn-primary" type="button">Strategic</button>](strategic/)
