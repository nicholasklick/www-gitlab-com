---
layout: handbook-page-toc
title: PII Removal Requests
category: GitLab.com
subcategory: Legal
description: General guide to resolve personally identifiable information removal requests
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Personally Identifiable Information removal requests

## Overview

Personally Identifiable Information (PII) is any data that could potentially identify a specific individual. Any information that can be used to distinguish one person from another. PII can be sensitive or non-sensitive. Non-sensitive PII is information from public records, phone books, corporate directories and websites without resulting in harm to the individual. Sensitive PII is information which, when disclosed, could result in harm to the individual whose privacy has been breached. This can be information publicly accessible via a GitLab.com

Keywords:  PII, Name, Social Security Number, Date and Place of birth, Mother's Maiden Name, Biometric Record, Résumé, Education Information etc.

PII requests have a 48 hour SLA.

## Workflow

1. Vet the request.
1. Verify that the reported links to the content is present in the request?
1. Is the content still live?
1. If the content is no longer available. Notify the requester and close the request. (cc abuse-reports@gitlab.com)
1. If the content is still live, forward the request to Abuse for further review.
1. The Abuse Team will comment on the ticket advising on the next steps or steps taken.

It is important for the Abuse team to review the request before forwarding it to the reported account.  The Abuse Team will create a new ticket and forward the initial notice, including the reference ticket in Internal Notes. Abuse Team will take over from here.
