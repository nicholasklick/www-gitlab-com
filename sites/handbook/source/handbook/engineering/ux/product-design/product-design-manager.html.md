---
layout: handbook-page-toc
title: "Product Design Manager Workflows"
description: "We support the business of GitLab by becoming experts in our stage group, educating ourselves about the entire product, and staying engaged with user and business goals. This page contains workflows specifix to Product Design Managers"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
{::options parse_block_html="true" /}

- TOC
{:toc .hidden-md .hidden-lg}


# Product Design Manager Workflows

## Team Skills Matrix

We conduct an annual team skills analysis based on a [process recommended by Nielsen-Norman](https://www.nngroup.com/articles/skill-mapping/).

This allows us to see our strengths as a team and to identify team trainings that would benefit a large portion of the team, help us identify strengths we'd like to add when hiring new team members, and inform individual team member's growth plans. It can also act as a resource for matching mentors and/or design pairs.

#### How to conduct the team assessment
1. Make a copy of the [GitLab focused template (private to GitLab)](https://docs.google.com/spreadsheets/d/1mN8Shag6X7xpikxnoJYm-IfWbuhgGe9pRiFm_SvCDD4/edit#gid=0). 
2. Review the template to make sure it still makes sense for your team. Do consider changes carefully and discuss with other managers. We want to be able to have consistency in these so we can view them across multiple design teams. 
3. Introduce this process to your team by sharing this page, the NN article and the GitLab focused template.
3. Copy the template for each individual team member and share it with them.
4. In 1:1s, have a casual conversation with the team member and ask them to rate themselves in their individual file. You may suggest adjustments based on your observations - if you do, share an example of why.
4. After completed the assessment with each team member, copy the results to the main team template. I kept this file private.
5. Create an issue similar to [#1674](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1674) to share the results. You can remove individual names from this view.
5. Have a retrospective with your team to identify strengths and weaknesses. Find things to build up the weaknesses (where people have said they want to improve).
5. Share with other UX leaders.


#### Definitions
Note: These are also in the template, which is the SSOT. 

##### Levels
0. Awareness: You are aware of the competency but are unable to perform tasks.
1.  Capable but still new to the skill: You understand the concept and you can apply it but your experience is limited  (you might be new to GitLab or new to quant research for example).
2. Intermediate proficiency: You have applied this skill to situations occasionally without needing guidance.
3. Advanced proficiency: You regularly apply this skill at an advanced level without needing guidance.
4. Advanced proficiency + leader: You can coach others in the application by explaining related nuances.
5. Expert: You have demonstrated consistent excellence across multiple projects and are a thought leader across the UX department.

##### Skills
* Qualitative research: Usability testing, interviewing. Creating effective unbiased research plans. Synthesizing data into insigts.
* Quantitative research: Understanding when to use surveys and how to use the data, AB testing - when to use and what do do the data, how to design for an experiment.
* Visual design - UI design, graphic design, illustrations, typography, logos.
* Interaction design - Form and flow design, micro-interactions, handling latency and error states, UI copy, page layout.
* Leadership: Starting new issues, being a content or design contributor (handbook, Pajamas, YouTube, etc), collaborating with others to acheive big goals, evangelizing your teams outcomes or sharing new methods or skills.
* Iteration: Every MVC is an iteration. Every iteration should be an MVC. Additionally, includes breaking projects down into parts to get results faster - could be smaller studies, smaller design scope. Not refining design work too early, not going too far into the work before circling back for feedback.
* Design communication: Sync or async, the ability to see and quickly understand a design, critique it, share design rationale for the design, get feedback from the right people at the right time.
* IA and Workflow Definition: Organizing large amounts of data or concepts, navigation design, card sort, and tree studies, creating workflow diagrams and screen flows.




