---
layout: markdown_page
title: "Contributing to Documentation"
description: "This section pertains to documentation changes that are independent of other code/feature changes."
canonical_path: "/community/contribute/documentation/"
---

## Documentation

If you're updating documentation as part of developing code, go to the [Contributing to Development page](/community/contribute/development/index.html).

- View the [documentation issues curated specifically for new contributors](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=good%20for%20new%20contributors&label_name%5B%5D=Accepting%20merge%20requests&label_name%5B%5D=documentation).

  - Pick an issue you'd like to work on. In the issue, comment that you want to work on it, mention `@gl-docsteam` and they will assign it to you.
  - If another contributor has already chosen the issue, pick a different one to work on.

- If you don't find an issue you'd like to work on, you can still open merge requests.

  - Try installing and running the [Vale linting tool](https://docs.gitlab.com/ee/development/documentation/testing.html#vale)
  and fix issues it finds.
  - Select Edit at the bottom of any page on docs.gitlab.com.
  - Or just look through pages [in the `/doc` directory](https://gitlab.com/gitlab-org/gitlab/-/tree/master/doc)
  until you find one you'd like to improve.

If you've never contributed to the GitLab documentation before, [view the instructions](https://docs.gitlab.com/ee/development/documentation/workflow.html).
